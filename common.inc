<?php
/**
 * Common functions and hook implementations.
 */

/**
 * Implements hook_requirements.
 * This module conflicts with anu_panels and Acton theme below ver 2.
 */
function acton_layouts_requirements($phase) {
  $t = get_t();

  switch ($phase) {
    case 'install':
    case 'runtime':
      // Check existence of ANU Panels
      if (module_exists('anu_panels')) {
        $requirements['acton_layouts'] = array(
          'title' => $t('Acton Layouts'),
          'description' => $t('Acton Layouts conflicts with ANU Panels.'),
          'severity' => REQUIREMENT_ERROR,
        );
      }

      // Check version of Acton theme
      $themes = list_themes();
      if (is_object($themes['acton']) && $themes['acton']->status) {
        $info = $themes['acton']->info;
        if (!empty($info['version']) && substr($info['version'], 4, 1) < 2) {
          $requirements['acton_layouts'] = array(
            'title' => $t('Acton Layouts'),
            'description' => $t('Acton Layouts only works with Acton theme version 2+.'),
            'severity' => REQUIREMENT_ERROR,
          );
        }
      }
      break;
  }

  if ($phase == 'runtime') {
    if (empty($requirements['acton_layouts'])) {
      $requirements['acton_layouts'] = array(
        'title' => $t('Acton Layouts'),
        'value' => $t('No conflicts detected.'),
        'severity' => REQUIREMENT_OK,
      );
    }
  }

  return $requirements;
}
