<?php

/**
 * Layout plugin definition.
 */
$plugin = array(
  'title' => t('ANU doublenarrow/narrow 1'),
  'category' => t('Columns: 2'),
  'icon' => 'anu_layout_1.png',
  'theme' => 'anu-layout-1',
  'panels' => array(
    'main' => t('Main column'),
    'right' => t('Sidebar')
  ),
);
