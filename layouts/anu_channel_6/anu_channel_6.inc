<?php

/**
 * Layout plugin definition.
 */
$plugin = array(
  'title' => t('ANU wide/wide 2'),
  'category' => t('Columns: 2'),
  'icon' => 'anu_channel_6.png',
  'theme' => 'anu_channel_6',
  'panels' => array(
    'row_1' => t('Row 1'),
    'row_2' => t('Row 2'),
    'left' => t('Left'),
    'right' => t('Right'),
  ),
);
