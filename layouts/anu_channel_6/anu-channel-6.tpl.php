<?php
/**
 * @file
 * Template for a 2 column panel layout with a header and footer.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['intro']: Content in the intro pane.
 *   - $content['search']: Content in the search pane.
 *   - $content['left']: Content in the left column.
 *   - $content['right']: Content in the right column.
 *   - $content['footer']: Content in the bottom pane.
 */
?>
<div class="panel-display clear-block" <?php if (!empty($css_id)): ?>id="<?php print $css_id; ?>"<?php endif; ?>>

  <?php 
  acton_layouts_render_pane($content['row_1'], 'doublewide');
  acton_layouts_render_pane($content['row_2'], 'doublewide');
 // acton_layouts_render_pane($content['row_2'], 'doublewide');
  acton_layouts_render_pane_row(array(
    '0' => array(
      'content' => $content['left'],
      'classes' => 'wide left first',
      ),
    '1' => array(
      'content' => $content['right'],
      'classes' => 'wide right last',
      ),
    ),
    $css_id
  );
  
  ?>

</div>