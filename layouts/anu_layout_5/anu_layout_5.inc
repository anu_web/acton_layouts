<?php

/**
 * Layout plugin definition.
 */
$plugin = array(
  'title' => t('ANU mixed 6'),
  'category' => t('Columns: 3'),
  'icon' => 'anu_layout_5.png',
  'theme' => 'anu-layout-5',
  'panels' => array(
    'top_left' => t('Top left'),
    'top_right' => t('Top right'),
    'middle_1' => t('Middle 1st'),
		'middle_2' => t('Middle 2nd'),
		'middle_3' => t('Middle 3rd'),
    'footer' => t('Footer'),
  ),
);
