<?php

/**
 * Layout plugin definition.
 */
$plugin = array(
  'title' => t('ANU mixed 3'),
  'category' => t('Columns: 2'),
  'icon' => 'anu_layout_13a.png',
  'theme' => 'anu_layout_13a',
  'panels' => array(
    'top_left' => t('Top main'),
    'top_right' => t('Top sidebar'),
    'middle' => t('Middle'),
    'bottom_left' => t('Bottom left'),
    'bottom_right' => t('Bottom right'),
  ),
);
