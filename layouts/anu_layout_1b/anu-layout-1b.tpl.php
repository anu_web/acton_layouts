<?php
/**
 * @file
 * Template for a 2 column panel layout.
 *
 * This template provides a two column panel display layout. The right column
 * floats within the main column, and disappears if there is no content/
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['main']: Content in the left column.
 *   - $content['right']: Content in the right column.
 */
?>
<div class="panel-display clear-block"<?php if (!empty($css_id)){ ?> id="<?php print $css_id; ?>"<?php }; ?>>
  <div class="panel-panel doublewide">
    <div class="inside">
    
    <?php if ($content['right']) { ?>
      <!-- begin right panel -->
      <div class="panel-panel narrow right last nopadtop" style="float:right">
        <div class="inside"><?php print $content['right']; ?></div>
      </div>
      <!-- end right panel -->
    <?php } ?>
    
    <?php print $content['main']; ?>
    
    </div>
  </div>
</div>
