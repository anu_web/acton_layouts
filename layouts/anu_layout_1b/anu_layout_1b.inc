<?php

/**
 * Layout plugin definition.
 */
$plugin = array(
  'title' => t('ANU doublenarrow/narrow 3'),
  'category' => t('Columns: 2'),
  'icon' => 'anu_layout_1b.png',
  'theme' => 'anu-layout-1b',
  'panels' => array(
    'main' => t('Main column'),
    'right' => t('Inset sidebar')
  ),
);
