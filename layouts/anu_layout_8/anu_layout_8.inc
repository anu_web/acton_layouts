<?php

/**
 * Layout plugin definition.
 */
$plugin = array(
  'title' => t('ANU mixed 4'),
  'category' => t('Columns: 3'),
  'icon' => 'anu_layout_8.png',
  'theme' => 'anu-layout-8',
  'panels' => array(
    'header' => t('Header'),
    'row_1_left' => t('Row 1 - Left column'),
    'row_1_right' => t('Row 1 - Right column'),
    'row_2_left' => t('Row 2 - Left column'),
    'row_2_center' => t('Row 2 - Center column'),
    'row_2_right' => t('Row 2 - Right column'),
  ),
);
