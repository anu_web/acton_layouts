<?php
/**
 * @file
 * Template for a 2 column panel layout with a header and footer.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['header']: Content in the top pane.
 *   - $content['row_1_left']: Content in the first row left column.
 *   - $content['row_1_right']: Content in the first row right column.
 *   - $content['row_2_left']: Content in the second row left column.
 *   - $content['row_2_center']: Content in the second row center column.
 *   - $content['row_2_right']: Content in the second row right column.
 */
?>
<div class="panel-display clear-block" <?php if (!empty($css_id)): ?>id="<?php print $css_id; ?>"<?php endif; ?>>

  <!-- top -->
  <?php 
  acton_layouts_render_pane_row(
    array(
      '0' => array(
        'content' => $content['header'],
        'classes' => 'doublewide',
      ),
    ),
    $css_id
  ); 
  ?>

  <!-- middle -->
  <?php 
  acton_layouts_render_pane_row(
    array(
      '0' => array(
        'content' => $content['row_1_left'],
        'classes' => 'wide left first',
      ),
      '1' => array(
        'content' => $content['row_1_right'],
        'classes' => 'wide left last',
      ),
    ),
    $css_id
  ); 
  ?>

	<!-- bottom -->
  <?php 
  acton_layouts_render_pane_row(
    array(
      '0' => array(
        'content' => $content['row_2_left'],
        'classes' => 'narrow left first',
      ),
      '1' => array(
        'content' => $content['row_2_center'],
        'classes' => 'narrow left',
      ),
      '2' => array(
        'content' => $content['row_2_right'],
        'classes' => 'narrow left last',
      ),
    ),
    $css_id
  ); 
  ?>
</div>
