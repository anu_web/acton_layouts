<?php

/**
 * Layout plugin definition.
 */
$plugin = array(
  'title' => t('ANU mixed 8'),
  'category' => t('Columns: 3'),
  'icon' => 'anu_layout_6.png',
  'theme' => 'anu-layout-6',
  'panels' => array(
    'top_left' => t('Top main'),
    'top_right' => t('Top sidebar'),
    'middle_1' => t('Middle 1st'),
		'middle_2' => t('Middle 2nd'),
		'middle_3' => t('Middle 3rd'),
    'bottom_left' => t('Bottom left'),
		'bottom_right' => t('Bottom right'),
  ),
);
