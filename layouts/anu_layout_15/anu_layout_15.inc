<?php

/**
 * Layout plugin definition.
 */
$plugin = array(
  'title' => t('ANU wide/wide 3'),
  'category' => t('Columns: 2'),
  'icon' => 'anu_layout_15.png',
  'theme' => 'anu_layout_15',
  'panels' => array(
    'top' => t('Top'),
    'above_left' => t('Above left'),
    'above_right' => t('Above right'),
    'middle' => t('Middle'),
    'below_left' => t('Below left'),
    'below_right' => t('Below right'),
    'bottom' => t('Bottom')
  ),
);
