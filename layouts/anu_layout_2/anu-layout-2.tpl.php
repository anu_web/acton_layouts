<?php
/**
 * @file
 * Template for a 2 column panel layout with 2 nested panels.
 *
 * This template provides a two column panel display layout. The left
 * column has two nested panels
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['main']: Content in the left column top pane.
 *   - $content['main_left']: Content in the left column left pane.
 *   - $content['main_right']: Content in the left column right pane.
 *   - $content['right']: Content in the right column.
 */
?>
<div class="panel-display clear-block" <?php if (!empty($css_id)): ?>id="<?php print $css_id; ?>"<?php endif; ?>>
  <!-- Main column -->
  <div class="left doublenarrow nopadtop first">
  
    <?php 
    acton_layouts_render_pane_row(
      array(
        '0' => array(
          'content' => $content['main'],
          'classes' => 'doublenarrow nopadtop first',
        ),
      ),
      $css_id
    ); 
    ?>
    
    <?php 
    acton_layouts_render_pane_row(
      array(
        '0' => array(
          'content' => $content['main_left'],
          'classes' => 'narrow left first',
        ),
        '1' => array(
          'content' => $content['main_right'],
          'classes' => 'narrow left last',
        ),
      ),
      $css_id
    ); 
    ?>
    
  </div>

  <?php 
  acton_layouts_render_pane_row(
    array(
      '0' => array(
        'content' => $content['right'],
        'classes' => 'narrow right nopadtop last',
      ),
    ),
    $css_id
  ); 
  ?>
</div>
