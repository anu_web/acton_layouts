<?php

/**
 * Layout plugin definition.
 */
$plugin = array(
  'title' => t('ANU mixed 9'),
  'category' => t('Columns: 3'),
  'icon' => 'anu_layout_2.png',
  'theme' => 'anu-layout-2',
  'panels' => array(
    'main' => t('Left column top pane'),
    'main_left' => t('Left column left pane'),
    'main_right' => t('Left column right pane'),
    'right' => t('Right column')
  ),
);
