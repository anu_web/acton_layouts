<?php

/**
 * Layout plugin definition.
 */
$plugin = array(
  'title' => t('ANU mixed 1'),
  'category' => t('Columns: 2'),
  'icon' => 'anu_layout_14.png',
  'theme' => 'anu_layout_14',
  'panels' => array(
    'header' => t('Header'),
    'left' => t('Left column'),
    'right' => t('Right column'),
    'bottom_left' => t('Bottom left'),
    'bottom_right' => t('Bottom right')
  ),
);
