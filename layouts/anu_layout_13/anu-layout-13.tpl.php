<?php
/**
 * @file
 * Template for a 2 column panel layout with a header and footer.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['header']: Content in the top pane.
 *   - $content['left']: Content in the left column.
 *   - $content['right']: Content in the right column.
 *   - $content['footer']: Content in the bottom pane.
 */
?>
<div class="panel-display clear-block" <?php if (!empty($css_id)): ?>id="<?php print $css_id; ?>"<?php endif; ?>>

  <?php 
  acton_layouts_render_pane_row(
    array(
      '0' => array(
        'content' => $content['top_left'],
        'classes' => 'narrow left first',
      ),
      '1' => array(
        'content' => $content['top_right'],
        'classes' => 'doublenarrow left last',
      ),
    ),
    $css_id
  ); 
  ?>

  <?php 
  acton_layouts_render_pane_row(
    array(
      '0' => array(
        'content' => $content['middle'],
        'classes' => 'doublewide',
      ),
    ),
    $css_id
  ); 
  ?>

  <?php 
  acton_layouts_render_pane_row(
    array(
      '0' => array(
        'content' => $content['bottom_left'],
        'classes' => 'wide left first',
      ),
      '1' => array(
        'content' => $content['bottom_right'],
        'classes' => 'wide left last',
      ),
    ),
    $css_id
  ); 
  ?>
</div>
