<?php

/**
 * Layout plugin definition.
 */
$plugin = array(
  'title' => t('ANU narrow/doublenarrow 1'),
  'category' => t('Columns: 2'),
  'icon' => 'anu_layout_16.png',
  'theme' => 'anu_layout_16',
  'panels' => array(
    'top' => t('Top'),
    'middle_left' => t('Middle main'),
    'middle_right' => t('Middle sidebar'),
    'bottom_left' => t('Bottom main'),
    'bottom_right' => t('Bottom sidebar'),
  ),
);
