<?php
/**
 * @file
 * Template for a 2 column panel layout, with conditional 2nd column.
 *
 * This template provides a two column panel display layout. The left
 * column is double narrow and the right column is narrow, if the right
 * column exists, otherwise the page is full-width.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['main']: Content in the left column.
 *   - $content['right']: Content in the right column.
 */

?>
<div class="panel-display clear-block" <?php if (!empty($css_id)): ?>id="<?php print $css_id; ?>"<?php endif; ?>>
  
  <?php if ($content['right']) { ?>
    <?php 
    acton_layouts_render_pane_row(
      array(
        '0' => array(
          'content' => $content['main'],
          'classes' => 'doublenarrow left first',
        ),
        '1' => array(
          'content' => $content['right'],
          'classes' => 'narrow left last',
        ),
      ),
      $css_id
    ); 
    ?>
  <?php } else { ?>
    
    <?php 
    acton_layouts_render_pane_row(
      array(
        '0' => array(
          'content' => $content['main'],
          'classes' => 'doublewide',
        )
      ),
      $css_id
    ); 
    ?>
    
  <?php } ?>
</div>
