<?php

/**
 * Layout plugin definition.
 */
$plugin = array(
  'title' => t('ANU doublenarrow/narrow 2'),
  'category' => t('Columns: 2'),
  'icon' => 'anu_layout_1a.png',
  'theme' => 'anu-layout-1a',
  'panels' => array(
    'main' => t('Main column'),
    'right' => t('Right column (conditional)')
  ),
);
