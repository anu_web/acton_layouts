<?php

/**
 * Layout plugin definition.
 */
$plugin = array(
  'title' => t('ANU doublenarrow/narrow 4'),
  'category' => t('Columns: 2'),
  'icon' => 'anu_layout_10.png',
  'theme' => 'anu-layout-10',
  'panels' => array(
    'header' => t('Header'),
    'left' => t('Left column'),
    'right' => t('Right column'),
    'footer' => t('Footer'),
  ),
);

