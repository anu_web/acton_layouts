<?php

/**
 * Layout plugin definition.
 */
$plugin = array(
  'title' => t('ANU wide/wide 1'),
  'category' => t('Columns: 2'),
  'icon' => 'anu_layout_4.png',
  'theme' => 'anu-layout-4',
  'panels' => array(
    'header' => t('Header'),
    'left' => t('Left column'),
    'right' => t('Right column'),
    'footer' => t('Footer'),
  ),
);
