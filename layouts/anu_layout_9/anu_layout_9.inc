<?php

/**
 * Layout plugin definition.
 */
$plugin = array(
  'title' => t('ANU mixed 5'),
  'category' => t('Columns: 3'),
  'icon' => 'anu_layout_9.png',
  'theme' => 'anu-layout-9',
  'panels' => array(
    'header' => t('Header'),
    'row_1_left' => t('Row 1 - Left column'),
    'row_1_right' => t('Row 1 - Right column'),
    'middle' => t('Middle'),
    'row_2_left' => t('Row 2 - Left column'),
    'row_2_center' => t('Row 2 - Center column'),
    'row_2_right' => t('Row 2 - Right column'),
    'footer' => t('Footer'),
  ),
);
