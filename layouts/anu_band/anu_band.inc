<?php

/**
 * Layout plugin definition.
 */
$plugin = array(
  'title' => t('ANU bands'),
  'category' => t('Columns: 1'),
  'icon' => 'anu_band.png',
  'theme' => 'anu_band',
  'css' => 'anu_band.css',
  'settings form' => 'anu_band_layout_settings_form',
  'settings validate' => 'anu_band_layout_settings_validate',
  'regions function' => 'anu_band_layout_regions',
);

function anu_band_layout_settings_form($display, $layout, $settings) {
  $settings += array(
    'hide_header' => TRUE,
    'count' => 1,
    'bands' => array(),
  );

  $form = array(
    '#type' => 'fieldset',
    '#title' => t('ANU bands'),
    '#collapsible' => $display->did != 'new',
    '#collapsed' => $display->did != 'new',
  );

  $form['hide_header'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide title and breadcrumb'),
    '#default_value' => $settings['hide_header'],
  );

  $form['count'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of bands'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#default_value' => $settings['count'],
    '#required' => TRUE,
    '#size' => 10,
  );

  for ($i = 1; $i <= $settings['count']; $i ++) {
    $id = 'band' . $i;
    $form['bands'][$id] = array(
      '#theme_wrappers' => array('container'),
      '#attributes' => array(
        'class' => array('anu-band-conf-band', 'clearfix'),
      ),
    );

    $settings['bands'] += array($id => array());
    $settings['bands'][$id] += array(
      'type' => 'white',
      'wrap_only' => FALSE,
      'block_center' => FALSE,
    );
    $form['bands'][$id]['type'] = array(
      '#type' => 'select',
      '#title' => t('Band @number type', array('@number' => $i)),
      '#options' => drupal_map_assoc(array(
        'banner',
        'home-banner',
        'promo',
        'uni2',
        'uni',
        'uni75',
        'uni50',
        'uni25',
        'uni10',
        'grey',
        'grey75',
        'grey50',
        'grey25',
        'grey10',
        'college',
        'college75',
        'college50',
        'college25',
        'college10',
        'black',
        'white',
      )),
      '#default_value' => $settings['bands'][$id]['type'],
      '#required' => TRUE,
    );

	  $form['bands'][$id]['style'] = array(
	    '#type' => 'textfield',
	    '#title' => t('Inline style'),
	    '#default_value' => $settings['bands'][$id]['style'],
	  );

    $form['bands'][$id]['wrap_only'] = array(
      '#type' => 'checkbox',
      '#title' => t('Wrapper only'),
      '#default_value' => $settings['bands'][$id]['wrap_only'],
    );
    $form['bands'][$id]['block_center'] = array(
      '#type' => 'checkbox',
      '#title' => t('Center blocks'),
      '#default_value' => $settings['bands'][$id]['block_center'],
    );
  }

  $form['#attached']['css'][] = array(
    'data' => '
      .anu-band-conf-band .form-item { float: left; margin-right: 1em; }
      .anu-band-conf-band .form-type-checkbox { padding-top: 1em; }
    ',
    'type' => 'inline',
  );
  return $form;
}

function anu_band_layout_settings_validate(&$values, $settings_form, $display, $layout, $settings) {
  // Prevent losing regions.
  $new_count = $values['count'];
  if (is_numeric($new_count) && $new_count > 0) {
    for ($i = 1; $i <= $settings['count']; $i++) {
      $id = 'band' . $i;
      if (!empty($display->panels[$id]) && $i > $new_count) {
        form_error($settings_form['count'], t('The number of bands cannot be set to less than the last band currently to contain content.'));
        break;
      }
    }
  }
}

function anu_band_layout_regions($display, $settings, $layout) {
  $settings += array('count' => 1, 'bands' => array());
  $count = max(1, $settings['count']);

  $regions = array();
  for ($i = 1; $i <= $count; $i ++) {
    $id = 'band' . $i;
    $settings['bands'] += array($id => array());
    $settings['bands'][$id] += array('type' => 'white');
    $type = $settings['bands'][$id]['type'];
    $regions[$id] = t('Band @number (@type)', array('@number' => $i, '@type' => $type));
  }

  return $regions;
}
