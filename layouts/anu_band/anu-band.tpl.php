<div class="panel-display acton-grid-layout anu-band-layout nopadtop clearfix"<?php if (!empty($css_id)) { ?> id="<?php print $css_id; ?>"<?php }; ?>>
<?php foreach ($content as $region => $region_content): ?>
  <?php if (!empty($region_content)): ?>
    <?php
    $type = !empty($settings['bands'][$region]['type']) ? $settings['bands'][$region]['type'] : 'white';

    $wrapper_attributes = array();
    if ($type == 'home-banner') {
      $wrapper_attributes['class'][] = 'band-banner-wrap';
      $wrapper_attributes['class'][] = 'band-banner-home';
    }
    else {
      $wrapper_attributes['class'][] = 'band-' . $type . '-wrap';
    }

    $wrapper_attributes['class'][] = 'clearfix';

    if (!empty($settings['bands'][$region]['style'])) {
      $wrapper_attributes['style'][] = $settings['bands'][$region]['style'];
    }

    $band_attributes = array();
    if (empty($settings['bands'][$region]['wrap_only'])) {
      if ($type == 'home-banner') {
        $band_attributes['class'][] = 'band-banner';
      }
      else {
        $band_attributes['class'][] = "band-$type";
      }
    }
    if (!empty($settings['bands'][$region]['block_center'])) {
      $band_attributes['class'][] = 'block-center';
    }
    ?>
    <div<?php echo drupal_attributes($wrapper_attributes); ?>>
      <div<?php echo drupal_attributes($band_attributes); ?>>
        <?php print $region_content; ?>
      </div>
    </div>
  <?php endif; ?>
<?php endforeach; ?>
</div>
