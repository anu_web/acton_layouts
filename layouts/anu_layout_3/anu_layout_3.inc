<?php

/**
 * Layout plugin definition.
 */
$plugin = array(
  'title' => t('ANU narrow/narrow/narrow 1'),
  'category' => t('Columns: 3'),
  'icon' => 'anu_layout_3.png',
  'theme' => 'anu-layout-3',
  'panels' => array(
    'header' => t('Header'),
    'left' => t('Left column'),
    'middle' => t('Middle column'),
    'right' => t('Right column'),
    'footer' => t('Footer'),
  ),
);
