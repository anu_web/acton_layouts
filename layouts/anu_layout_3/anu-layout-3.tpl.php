<?php
/**
 * @file
 * Template for a 3 column panel layout with a header and footer.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['header']: Content in the top pane.
 *   - $content['left']: Content in the left column.
 *   - $content['middle']: Content in the middle column.
 *   - $content['right']: Content in the right column.
 *   - $content['footer']: Content in the bottom pane.
 */
?>
<div class="panel-display clear-block" <?php if (!empty($css_id)): ?>id="<?php print $css_id; ?>"<?php endif; ?>>
  <!-- Header -->
  <?php acton_layouts_render_pane($content['header'], 'doublewide'); ?>
  
  <!-- Middle -->
  <?php 
  acton_layouts_render_pane_row(array(
    '0' => array(
      'content' => $content['left'],
      'classes' => 'narrow left first',
      ),
    '1' => array(
      'content' => $content['middle'],
      'classes' => 'narrow left',
      ),
    '2' => array(
      'content' => $content['right'],
      'classes' => 'narrow left last',
      ),
    )
  ); 
  ?>
  
  <!-- Footer -->
  <?php acton_layouts_render_pane($content['footer'], 'doublewide'); ?>
  
</div>
