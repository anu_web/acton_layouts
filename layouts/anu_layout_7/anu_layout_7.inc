<?php

/**
 * Layout plugin definition.
 */
$plugin = array(
  'title' => t('ANU mixed 7'),
  'category' => t('Columns: 3'),
  'icon' => 'anu_layout_7.png',
  'theme' => 'anu-layout-7',
  'panels' => array(
    'top_left' => t('Top main'),
    'top_right' => t('Top sidebar'),
    'middle_left' => t('Middle left'),
		'middle_right' => t('Middle right'),
		'bottom_1' => t('Bottom left'),
    'bottom_2' => t('Bottom middle'),
		'bottom_3' => t('Bottom right'),
  ),
);
