<?php
/**
 * @file
 * Template for a 2 column panel layout with a header and footer.
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['header']: Content in the top pane.
 *   - $content['left']: Content in the left column.
 *   - $content['right']: Content in the right column.
 *   - $content['footer']: Content in the bottom pane.
 */
 
  $item_class = NULL;
 
  // Selector magic. Set row classes dynamically based on CSS ID
 
 $item_class = 'normal';
 
  if($css_id){
    switch($css_id){
      case 'channel':
        $item_class = 'section';
        break;
      default:
        
    }
    $item_class .= ' row-'.$css_id;
  }

  // Build an array of rows omitting hidden rows
  $rendered_content = array();
  
  foreach ($content as $key => $row){
    if(!empty($row['content'])) {
      $rendered_content[$key]['content'] = $row;
      $rendered_content[$key]['id'] = $key;
      $rendered_content[$key]['class'] = $item_class;
    }
  }
  
  end($rendered_content);
  $last_key = key($rendered_content);
  $rendered_content[$last_key]['class'] .= ' last-row';
  
  reset($rendered_content);
  $first_key = key($rendered_content);
  $rendered_content[$first_key]['class'] .= ' first-row nopadtop';
  
?>
<div class="panel-display clear-block" <?php if (!empty($css_id)): ?>id="<?php print $css_id; ?>"<?php endif; ?>>

  <?php 
  // As every row is the same, a quick shorthand to output them...
  $count = 1;
  
  foreach($rendered_content as $row){ ?>
    <div id="<?php echo $row['id']; ?>" class="panel-panel nopadtop doublewide <?php echo $row['class']; ?>">
      <div class="inside"><?php echo $row['content']; ?></div>
    </div>
  <?php } ?>
  
  

</div>
