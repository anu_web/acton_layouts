<?php

/**
 * Layout plugin definition.
 */
$plugin = array(
  'title' => t('ANU wide/wide 3'),
  'category' => t('Columns: 2'),
  'icon' => 'anu_channel_4.png',
  'theme' => 'anu_channel_4',
  'panels' => array(
    'row_1' => t('Row 1'),
    'row_2' => t('Row 2'),
    'row_3' => t('Row 3'),
    'row_4' => t('Row 4'),
    'row_5' => t('Row 5'),
    'row_6' => t('Row 6'),
    'row_7' => t('Row 7'),
    'row_8' => t('Row 8'),
  ),
);
